import psycopg2
#execfile("config.py")
import config
#print ("Opened database successfully")
#-------------------------------------------------
def update_buy(buy_id,buy_id_new,time,date,total_price):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		update buy set(buy_id=%s,"time"=%S,"date"=%S,"total_price"=%S) where buy_id=%s
		''',(buy_id_new,time,date,total_price,buy_id))
	conn.commit()
	print ("buy updated successfully")
	conn.close() 
def update_cpu(model,model_new,frequency):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		update cpu set(model=%s,"frequency"=%s) where model=%s
		''',(model_new,frequency,model))
	conn.commit()
	print ("cpu updated successfully")
	conn.close() 	
def update_gpu(model,model_new,memory,shared_memory):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		update gpu set(model=%s,"memory"=%s,shared_memory=%s) where model=%s
		''',(model_new,memory,shared_memory,model))
	conn.commit()
	print ("gpu updated successfully")
	conn.close() 
def update_customer(customer_id,customer_id_new,name,telephone,address,personal_id):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		update customer set(customer_id=%s,name=%s,telephone=%s,address=%s,personal_id=%s) where customer_id=%s
		''',(customer_id_new,name,telephone,address,personal_id,customer_id))
	conn.commit()
	print ("customer updated successfully")
	conn.close() 
def update_laptop(model,brand,model_new,brand_new,price,color,cpu_model,gpu_model):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		update laptop set(model=%s,brand=%s,price=%s,color=%s,cpu_model=%s,gpu_model=%s)where model=%s & brand=%s
		''',(model_new,brand_new,price,color,cpu_model,gpu_model,model,brand))
	conn.commit()
	print ("laptop updated successfully")
	conn.close()  	
def update_transaction(buy_id,customer_id,model,brand,buy_id_new,customer_id_new,model_new,brand_new):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		update transaction set(buy_id=%s,customer_id=%s,model=%s,brand=%s) where buy_id=%s & customer_id=%s & model=%s and brand=%s
		''',(buy_id_new,customer_id_new,model_new,brand_new,buy_id,customer_id,model,brand))
	conn.commit()
	print ("transaction updated successfully")
	conn.close() 
#-------------------------------------------------
#update_buy(10,'11:13:51','2015-12-19',100)


