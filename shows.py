
import psycopg2

#execfile("config.py")
import config





def show_buy():
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		select * from buy
		''')
	print("show_buy")
	rows = cur.fetchall()
	for row in rows:
		print ("	",row)
	conn.close() 


def show_cpu():
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		select * from cpu
		''')
	print("show_cpu")
	rows = cur.fetchall()
	for row in rows:
		print ("	",row)
	conn.close() 	
	
def show_gpu():
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		select * from gpu
		''')
	print("show_gpu")
	rows = cur.fetchall()
	for row in rows:
		print ("	",row)
	conn.close() 
	
def show_customer():
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		select * from customer
		''')
	print("show_customer")
	rows = cur.fetchall()
	for row in rows:
		print ("	",row)
	conn.close() 
	
	
def show_laptop():
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		select * from laptop
		''')
	rows = cur.fetchall()
	print("show_laptop")
	for row in rows:
		print ("	",row)
	conn.close()  
	
def show_transaction():
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		select * from transaction
		''')
	rows = cur.fetchall()
	print("show_transaction")
	for row in rows:
		print ("	",row)
	conn.close() 

	

#show_buy()
#show_cpu()
#show_gpu()
#show_laptop()
#show_customer()
#show_transaction()


