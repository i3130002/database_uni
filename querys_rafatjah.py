import psycopg2

import config

def get_str(length):
	str=""
	str = input()
	while(len(str)>length)	:	
		print("size must be <=" ,length )
		str = input("")
	return str 
#1	
def select_laptop_by_cpu_model():
	print("inter cpu model:")
	cpu_model=get_str(10)
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where cpu_model=%s
		''',(cpu_model))
	#conn.commit()
	#print("model ,brand")
	print ("laptops having cpu:",cpu_model,'\n are\n model,brand\n',cur.fetchall())
	conn.close() 
#4	
def select_laptop_model_by_brand():
	print("inter laptop brand:")
	brand=get_str(10)
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model from laptop  
			where brand=%s
		''',(brand ))
	#conn.commit()
	print ("laptops with brand ",brand,' are\n model\n',cur.fetchall())
	conn.close() 
#5
def select_laptop_price_equal():
	
	price=int(input("enter price:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where price=%s
		''',(price ,))
	#conn.commit()
	print ("laptops with price equal than ",price,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#6
def select_laptop_price_grater_than():
	
	price=int(input("enter price:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where price>%s
		''',(price ,))
	#conn.commit()
	print ("laptops with price grater than ",price,' are\n model,brand\n',cur.fetchall())
	conn.close() 	
#7
def select_laptop_price_lower_than():
	
	price=int(input("enter price:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where price<%s
		''',(price ,))
	#conn.commit()
	print ("laptops with price lower than ",price,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#8
def select_laptop_price_between():
	
	price1=int(input("enter price lower:"))
	price2=int(input("enter price higher:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where price betwin %s and %s
		''',(price1 ,price2))
	#conn.commit()
	print ("laptops with price between  ",price1,' and ',price2,' are\n model,brand\n',cur.fetchall())
	conn.close() 

#10
def select_laptop_cpu_frequncy_equal():
	
	frequency=int(input("enter frequency:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where frequency=%s
		''',(frequency ,))
	#conn.commit()
	print ("laptops with frequency equal ",frequency,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#11
def select_laptop_cpu_frequncy_grater():
	
	frequency=int(input("enter frequency:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where frequency>%s
		''',(frequency ,))
	#conn.commit()
	print ("laptops with frequency grater ",frequency,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#12
def select_laptop_cpu_frequncy_lower():
	
	frequency=int(input("enter frequency:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where frequency<%s
		''',(frequency ,))
	#conn.commit()
	print ("laptops with frequency lower ",frequency,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#13
def select_laptop_cpu_frequncy_between():
	
	frequency1=int(input("enter frequency lower bound:"))
	frequency2=int(input("enter frequency upper bound:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where frequency BETWEEN %s and %s
		''',(frequency1,frequency2))
	#conn.commit()
	print ("laptops with frequency between ",frequency1,' and ',frequency2 ,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#16
def select_laptop_gpu_memory_lower_than():
	
	memory=int(input("enter memory upper bound:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join gpu on laptop.gpu_model=gpu.model
			where memory < %s 
		''',(memory,))
	#conn.commit()
	print ("laptops with memory lower than ",memory ,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#19
def select_laptop_color_by_model():
	print("enter laptop model:")
	model=get_str(10)
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select color,brand from laptop 
			where model=%s 
		''',(model,))
	#conn.commit()
	print ("laptops with model ",model ,' are colored as\n color,brand\n',cur.fetchall())
	conn.close() 
#22
def select_customer_numbers_by_customer_name():
	print("enter customer name:")
	name=get_str(10)
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select customer_id ,telephone,personal_id from customer 
			where name=%s 
		''',(name,))
	#conn.commit()
	#print("customer_id ,telephone,personal_id")
	print ("customer with name:",name ,' are have numbered as\n customer_id ,telephone,personal_id\n',cur.fetchall())
	conn.close()
#25
def select_laptops_in_buy_id():
	#print()
	transaction=int(input("enter buy_id:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select DISTINCT model ,brand from transaction 
			where buy_id=%s 
		''',(transaction,))
	#conn.commit()
	#print("model ,brand")
	print ("transaction with buy_id:",transaction ,' are\n model ,brand\n',cur.fetchall())
	conn.close()
#28
def select_transactions_by_customer_id():
	#print()
	customer_id=int(input("enter customer_id:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select DISTINCT buy_id from transaction 
			where customer_id=%s 
		''',(customer_id,))
	#conn.commit()
	#print("model ,brand")
	print ("transaction with customer_id:",customer_id ,' are\n',cur.fetchall())
	conn.close()

#select_laptop_price_lower_than()