import psycopg2
#execfile("config.py")
import config
#print ("Opened database successfully")
#-------------------------------------------------
def create_buy( ):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		CREATE TABLE IF NOT EXISTS buy (
		"time" time(6),
		"date" date,
		"total_price" int8,
		"buy_id" int8 NOT NULL,
		PRIMARY KEY ("buy_id")
		)
		;
		''' )
	conn.commit()
	print ("buy created or exists (done)")
	conn.close() 

def create_cpu( ):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		CREATE TABLE IF NOT EXISTS cpu (
		"model" varchar(10) COLLATE "default" NOT NULL,
		"frequency" float4,
		PRIMARY KEY ("model")
		)
		;
		''' )
	conn.commit()
	print ("buy created or exists (done)")
	conn.close() 

	
	




def create_gpu( ):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		CREATE TABLE IF NOT EXISTS gpu (
		"model" varchar(10) COLLATE "default" NOT NULL,
		"memory" int8,
		"shared_memory" int8,
		PRIMARY KEY ("model")
		)
		;
		''' )
	conn.commit()
	print ("buy created or exists (done)")
	conn.close() 

	
	



def create_customer( ):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		CREATE TABLE IF NOT EXISTS customer (
		"customer_id" int4 NOT NULL,
		"name" varchar(20) COLLATE "default",
		"telephone" varchar COLLATE "default",
		"address" varchar(50) COLLATE "default",
		"personal_id" int8,
		PRIMARY KEY ("customer_id")
		)
		;
		''' )
	conn.commit()
	print ("buy created or exists (done)")
	conn.close() 

	

def create_laptop( ):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		CREATE TABLE IF NOT EXISTS laptop (
		"model" varchar(10) COLLATE "default" NOT NULL,
		"brand" varchar(10) COLLATE "default" NOT NULL,
		"price" int8 NOT NULL,
		"color" varchar(10) COLLATE "default",
		"cpu_model" varchar(10) COLLATE "default" NOT NULL,
		"gpu_model" varchar(10) COLLATE "default" NOT NULL,
		PRIMARY KEY ("brand", "model"),
		CONSTRAINT cpu_model FOREIGN KEY ("cpu_model") REFERENCES cpu ("model") ON DELETE NO ACTION ON UPDATE NO ACTION,
		CONSTRAINT gpu_model FOREIGN KEY ("gpu_model") REFERENCES gpu ("model") ON DELETE NO ACTION ON UPDATE NO ACTION
		)
		;
		''' )
	conn.commit()
	print ("buy created or exists (done)")
	conn.close() 


def create_transaction( ):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		CREATE TABLE IF NOT EXISTS transaction (
		"buy_id" int8 NOT NULL,
		"customer_id" int8 NOT NULL,
		"model" varchar COLLATE "default" NOT NULL,
		"brand" varchar COLLATE "default" NOT NULL,
		PRIMARY KEY ("buy_id", "customer_id", "model", "brand"),
		CONSTRAINT customer_id FOREIGN KEY  ("customer_id") REFERENCES "customer" ("customer_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
		CONSTRAINT buy_id FOREIGN KEY  ("buy_id") REFERENCES "buy" ("buy_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
		CONSTRAINT model_barand FOREIGN KEY ("model", "brand") REFERENCES "laptop" ("model", "brand") ON DELETE NO ACTION ON UPDATE NO ACTION
		)
		;
		''' )
	conn.commit()
	print ("buy created or exists (done)")
	conn.close() 

	
#-------------------------------------------------
create_buy()
create_cpu()
create_gpu()
create_customer()
create_laptop()
create_transaction()



