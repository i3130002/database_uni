import psycopg2
#execfile("config.py")
import config
#-------------------------------------------------
def delete_buy(buy_id):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
			delete from buy where buy_id=%s
		''',(buy_id,))
	conn.commit()
	print ("delete from buy successfully")
	conn.close()
def delete_cpu(model):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
			delete from cpu where model=%s
		''',(model,))
	conn.commit()
	print ("delete from cpu successfully")
	conn.close()
def delete_gpu(model):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
			delete from gpu where model=%s
		''',(model,))
	conn.commit()
	print ("delete from gpu successfully")
	conn.close()
def delete_customer(customer_id):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
			delete from customer where customer_id=%s
		''',(customer_id,))
	conn.commit()
	print ("delete from customer successfully")
	conn.close()
def delete_laptop(model , brand):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
			delete from laptop where model=%s and brand=%s
		''',(model,brand))
	conn.commit()
	print ("delete from laptop successfully")
	conn.close()
def delete_transaction(buy_id , customer_id , model , brand):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
			delete from transaction where buy_id=%s and customer_id=%s and model=%s and brand=%s
		''',(buy_id , customer_id , model , brand ))
	conn.commit()
	print ("delete from transaction successfully")
	conn.close()
#-------------------------------------------------
#delete_customer(1001)
#delete_gpu('Asus')
#delete_cpu('Asus')
#delete_laptop('as1', 'asus')
#delete_transaction(1,1001,'as1','asus')
#DELETE FROM [ ONLY ] table [ * ] [ [ AS ] alias ]
#    [ USING using_list ]
#   [ WHERE condition | WHERE CURRENT OF cursor_name ]
#   [ RETURNING * | output_expression [ [ AS ] output_name ] [, ...] ]