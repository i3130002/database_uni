import psycopg2
import config
#--------------------
def get_str(length):
	str=""
	str = input()
	while(len(str)>length)	:	
		print("size must be <=" ,length )
		str = input("")
	return str 
#1	R
def select_laptop_by_cpu_model():
	print("inter cpu model:")
	cpu_model=get_str(10)
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where cpu_model=%s
		''',(cpu_model))
	#conn.commit()
	#print("model ,brand")
	print ("laptops having cpu:",cpu_model,'\n are\n model,brand\n',cur.fetchall())
	conn.close() 
#2	M
def select_laptop_by_gpu_model():
	print("inter gpu model:")
	gpu_model=get_str(10)
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join gpu on laptop.gpu_model=gpu.model
			where gpu_model=%s
		''',(gpu_model))
	#conn.commit()
	#print("model ,brand")
	print ("laptops having gpu:",gpu_model,'\n are\n model,brand\n',cur.fetchall())
	conn.close() 
#3	M
def select_laptop_by_model():
	print("enter laptop model:")
	model=get_str(10)
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop 
			where model=%s
		''',(model))
	#conn.commit()
	#print("model ,brand")
	print ("laptops with:",model,'\n are\n model,brand\n',cur.fetchall())
	conn.close() 
#4	R
def select_laptop_model_by_brand():
	print("inter laptop brand:")
	brand=get_str(10)
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model from laptop  
			where brand=%s
		''',(brand ))
	#conn.commit()
	print ("laptops with brand ",brand,' are\n model\n',cur.fetchall())
	conn.close() 
#5	R
def select_laptop_price_equal():
	
	price=int(input("enter price:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where price=%s
		''',(price ,))
	#conn.commit()
	print ("laptops with price equal than ",price,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#6	R
def select_laptop_price_grater_than():
	
	price=int(input("enter price:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where price>%s
		''',(price ,))
	#conn.commit()
	print ("laptops with price grater than ",price,' are\n model,brand\n',cur.fetchall())
	conn.close() 	
#7	R
def select_laptop_price_lower_than():
	
	price=int(input("enter price:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where price<%s
		''',(price ,))
	#conn.commit()
	print ("laptops with price lower than ",price,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#8	R
def select_laptop_price_between():
	
	price1=int(input("enter price lower:"))
	price2=int(input("enter price higher:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where price betwin %s and %s
		''',(price1 ,price2))
	#conn.commit()
	print ("laptops with price between  ",price1,' and ',price2,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#9	M
def select_laptop_by_color():
	
	color=int(input("enter color:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select model,brand from laptop  
			where color=%s
		''',(color ,))
	#conn.commit()
	print ("laptops with color ",color,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#10	R
def select_laptop_cpu_frequncy_equal():
	
	frequency=int(input("enter frequency:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where frequency=%s
		''',(frequency ,))
	#conn.commit()
	print ("laptops with frequency equal ",frequency,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#11	R
def select_laptop_cpu_frequncy_grater():
	
	frequency=int(input("enter frequency:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where frequency>%s
		''',(frequency ,))
	#conn.commit()
	print ("laptops with frequency grater ",frequency,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#12	R
def select_laptop_cpu_frequncy_lower():
	
	frequency=int(input("enter frequency:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where frequency<%s
		''',(frequency ,))
	#conn.commit()
	print ("laptops with frequency lower ",frequency,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#13	R
def select_laptop_cpu_frequncy_between():
	
	frequency1=int(input("enter frequency lower bound:"))
	frequency2=int(input("enter frequency upper bound:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join cpu on laptop.cpu_model=cpu.model
			where frequency BETWEEN %s and %s
		''',(frequency1,frequency2))
	#conn.commit()
	print ("laptops with frequency between ",frequency1,' and ',frequency2 ,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#14	M
def select_laptop_gpu_memory():
	
	memory=int(input("enter memory:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join gpu on laptop.gpu_model=gpu.model
			where memory = %s 
		''',(memory,))
	#conn.commit()
	print ("laptops with memory  ",memory ,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#15	M
def select_laptop_gpu_memory_upper_than():
	
	memory=int(input("enter memory lower bound:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join gpu on laptop.gpu_model=gpu.model
			where memory > %s 
		''',(memory,))
	#conn.commit()
	print ("laptops with memory upper than ",memory ,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#16	R
def select_laptop_gpu_memory_lower_than():
	
	memory=int(input("enter memory upper bound:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join gpu on laptop.gpu_model=gpu.model
			where memory < %s 
		''',(memory,))
	#conn.commit()
	print ("laptops with memory lower than ",memory ,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#17 M
def select_laptop_gpu_memory_between():
	memory1=int(input("enter memory lower bound:"))
	memory2=int(input("enter memory upper bound:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select laptop.model,brand from laptop join gpu on laptop.gpu_model=gpu.model
			where memory BETWEEN %s and %s 
		''',(memory1,memory2))
	#conn.commit()
	print ("laptops with memory between ",memory1,' and ',memory2 ,' are\n model,brand\n',cur.fetchall())
	conn.close() 
#18 M
def select_laptop_price_by_model():
	print("enter laptop model:")
	model=get_str(10)
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select price,brand from laptop 
			where model=%s 
		''',(model,))
	#conn.commit()
	print ("laptops with model ",model ,' are priced as\n price,brand\n',cur.fetchall())
	conn.close() 
#19	R
def select_laptop_color_by_model():
	print("enter laptop model:")
	model=get_str(10)
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select color,brand from laptop 
			where model=%s 
		''',(model,))
	#conn.commit()
	print ("laptops with model ",model ,' are colored as\n color,brand\n',cur.fetchall())
	conn.close() 
#20 M
def select_name_by_customer_id():
	id = int(input("enter customer_id:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select name from customer 
			where customer_id=%s 
		''',(id,))
	#conn.commit()
	#print("customer_id ,telephone,personal_id")
	print ("customer with customer_id:",id ,' are have name as \n',cur.fetchall())
	conn.close()
#21 M
def select_address_by_customer_name():
	print("enter customer name:")
	name=get_str(20)
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select customer_id ,address from customer 
			where name=%s 
		''',(name,))
	#conn.commit()
	#print("customer_id ,telephone,personal_id")
	print ("customer with name:",name ,' are have customer_id ,address\n',cur.fetchall())
	conn.close()
#22	R
def select_customer_numbers_by_customer_name():
	print("enter customer name:")
	name=get_str(10)
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select customer_id ,telephone,personal_id from customer 
			where name=%s 
		''',(name,))
	#conn.commit()
	#print("customer_id ,telephone,personal_id")
	print ("customer with name:",name ,' are have numbered as\n customer_id ,telephone,personal_id\n',cur.fetchall())
	conn.close()
#23 M	!!!
def insert_transaction():
	customer_id = int(input("enter customer_id:"))
	buy_id = int(input("enter customer_id:"))
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	x=1
	while ( x!= 0 ):
		print("enter model:")
		model=get_str(10)
		print("enter brand:")
		brand=get_str(10)
		cur.execute('''
			insert into transaction (buy_id,customer_id,model,brand) values(%s,%s,%s,%s)
			''',(buy_id,customer_id,model,brand))
		x = int(input("if you don't want add another laptop with by entered buy_id and customer_id press 0"))

	conn.commit()
	print ("transaction added successfully")
	conn.close() 
#24 M
def select_laptops_in_buy_id():
	#print()
	transaction=int(input("enter buy_id:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select DISTINCT customer_id from transaction 
			where buy_id=%s 
		''',(transaction,))
	#conn.commit()
	#print("model ,brand")
	print ("transaction with buy_id:",transaction ,' are\n name \n',cur.fetchall())
	conn.close()
#25	R
def select_laptops_by_buy_id():
	#print()
	transaction=int(input("enter buy_id:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select DISTINCT model ,brand from transaction 
			where buy_id=%s 
		''',(transaction,))
	#conn.commit()
	#print("model ,brand")
	print ("transaction with buy_id:",transaction ,' are\n model ,brand\n',cur.fetchall())
	conn.close()
#26 M
def select_total_price_by_buy_id():
	transaction=int(input("enter buy_id:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select total_price from buy
			where buy_id=%s 
		''',(transaction,))
	#conn.commit()
	#print("model ,brand")
	print ("transaction with buy_id:",transaction ,' are\n model ,brand\n',cur.fetchall())
	conn.close()
#27	M
def select_laptop_gpu_memory_between():
	price1=int(input("enter price lower bound:"))
	price2=int(input("enter price upper bound:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select buy_id from laptop join transaction
			where price BETWEEN %s and %s 
		''',(price1,price2))
		#cur.execute('''		 select buy_id from laptop join transaction on laptop.gpu_model=gpu.model			where memory BETWEEN %s and %s 		''',(memory1,memory2))
	#conn.commit()
	print ("laptops with price between ",price1,' and ',price2 ,' are\n buy_id\n',cur.fetchall())
	conn.close() 
#28	R
def select_transactions_by_customer_id():
	#print()
	customer_id=int(input("enter customer_id:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select DISTINCT buy_id from transaction 
			where customer_id=%s 
		''',(customer_id,))
	#conn.commit()
	#print("model ,brand")
	print ("transaction with customer_id:",customer_id ,' are\n',cur.fetchall())
	conn.close()
#29 M
def select_tranaction_by_buy_id():
	transaction=int(input("enter buy_id:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select customer_id,model,brand from transaction 
			where buy_id=%s 
		''',(transaction,))
	#conn.commit()
	#print("model ,brand")
	print ("transaction with buy_id:",transaction ,' are\n customer_id,model,brand \n',cur.fetchall())
	conn.close()
#30 M
def select_customer_id_by_buy_id():
	transaction=int(input("enter buy_id:"))
	 
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		 select customer_id from transaction 
			where buy_id=%s 
		''',(transaction,))
	#conn.commit()
	#print("model ,brand")
	print ("transaction with buy_id:",transaction ,' are\n customer_id \n',cur.fetchall())
	conn.close()
#select_laptop_price_lower_than()