import psycopg2
#execfile("config.py")
import config
#print ("Opened database successfully")
#-------------------------------------------------
def insert_buy(buy_id,time,date,total_price):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		insert into buy ("buy_id","time","date","total_price") values(%s,%s,%s,%s)
		''',(buy_id,time,date,total_price))
	conn.commit()
	print ("buy added successfully")
	conn.close() 
def insert_cpu(model,frequency):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		insert into cpu ("model","frequency") values(%s,%s)
		''',(model,frequency))
	conn.commit()
	print ("cpu added successfully")
	conn.close() 	
def insert_gpu(model,memory,shared_memory):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		insert into gpu ("model","memory",shared_memory) values(%s,%s,%s)
		''',(model,memory,shared_memory))
	conn.commit()
	print ("gpu added successfully")
	conn.close() 	
def insert_customer(customer_id,name,telephone,address,personal_id):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		insert into customer (customer_id,name,telephone,address,personal_id) values(%s,%s,%s,%s,%s)
		''',(customer_id,name,telephone,address,personal_id))
	conn.commit()
	print ("customer added successfully")
	conn.close() 
def insert_laptop(model,brand,price,color,cpu_model,gpu_model):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		insert into laptop (model,brand,price,color,cpu_model,gpu_model) values(%s,%s,%s,%s,%s,%s)
		''',(model,brand,price,color,cpu_model,gpu_model))
	conn.commit()
	print ("laptop added successfully")
	conn.close()  
def insert_transaction(buy_id,customer_id,model,brand):
	conn = psycopg2.connect(database=config.database_, user=config.user_, password=config.password_, host=config.host_, port=config.port_)
	cur = conn.cursor()
	cur.execute('''
		insert into transaction (buy_id,customer_id,model,brand) values(%s,%s,%s,%s)
		''',(buy_id,customer_id,model,brand))
	conn.commit()
	print ("transaction added successfully")
	conn.close() 
#-------------------------------------------------
#insert_buy(10,'11:13:51','2015-12-19',100)


